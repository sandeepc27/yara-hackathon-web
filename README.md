# Yara Hackathon


## To Create folder
```
mkdir Yara-Hackathon
```

![alt text](https://drive.google.com/uc?export=view&id=1VUsK97WEdaUW4rCa2638SY5jSFjdHKOF)

## To clone the project 
```
git clone https://gitlab.com/sandeepc27/yara-hackathon-web.git
```

![alt text](https://drive.google.com/uc?export=view&id=1zxurcjLnJpXQCvyu4v-t-CdkqlqQN0Xt)

## To install yarn
```
sudo npm install -g yarn
```

![alt text](https://drive.google.com/uc?export=view&id=1Gn9SrpEVZixTZC_OfhcoTCU2hzpWQ13p)

## Move to the sub directory

![alt text](https://drive.google.com/uc?export=view&id=1aKgEDA3rrHhEg5AtBU4v7UGYIwnWaGZj)

## install necessary Dependencies
```
yarn add infinite-react-carousel
yarn add react-router-dom
yarn add react-split-pane
yarn add react-tabs
```

## To Run the project 
```
yarn start
```

![alt text](https://drive.google.com/uc?export=view&id=1EIzEVedORWrI_ruOmYfGhUENeVWHygM-)

## Execution in chrome

![alt text](https://drive.google.com/uc?export=view&id=1ioAdjYSiezleGQutlheQ_R-nREwkgXc5)


## UseCase diagram
![alt text](https://drive.google.com/uc?export=view&id=1w1RtYno-8WKfNv_xyyivNxnzYmJgxhXi)

## Sequence diagram
Flow of execution of our project

![alt text](https://drive.google.com/uc?export=view&id=165R-LYGItuC-ZsEtCmO7Fuv6-kZUx8jl)
