const apiEndPoint = 'http://localhost:2000/api/team'

export const getTeam = async () => {
    return fetch(apiEndPoint, {
     method: 'GET', 
      headers: {'Content-Type': 'application/json'},
      })
      .then((response) => response.json())
      .then((data) => {return data;})
      .catch((error) => { console.error('Error:', error); 
    });
  };

  export const addTeam = async (record) =>{
    return fetch(apiEndPoint, {
      method: 'POST',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
          body:JSON.stringify(record)
      })
    .then(response => response.json())
    .then(response => { return response;})
    .catch((error) => { console.log(error); });
  }

  export const getTeamById = async (id) =>{
    return fetch(apiEndPoint + "/"+id, {
      method: 'GET',
          headers: {'Content-Type': 'application/json;charset=utf-8'}
      })
    .then(response => response.json())
    .then(response => { return response; })
    .catch((error) => { console.log(error); });
  }

  export const getTeamByHackthon = async (id) =>{
    return fetch(apiEndPoint+"/hackathon/"+id, {
      method: 'GET',
          headers: {'Content-Type': 'application/json;charset=utf-8'}
      })
    .then(response => response.json())
    .then(response => {console.log(response); return response; })
    .catch((error) => { console.log(error); });
  }

  export const updateTeam = async (record) =>{
    return fetch(apiEndPoint, {
      method: 'PUT',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
          body:JSON.stringify(record)
      })
    .then(response => response.json())
    .then(response => {  return response; })
    .catch((error) => { console.log(error); });
  } 

  export const getEmployeeBySearch = async (field, searchText) => {
    return fetch(apiEndPoint + '/search/' + field + '/' + searchText, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    })
    .then((response) => response.json())
    .then((data) => { 
      console.log("data is",data)
      return data; })
    .catch((err) => { console.log(err) });
}

  export const deleteTeam = async (record) =>{
    return fetch(apiEndPoint+"/"+record, {
      method: 'DELETE',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
      })
    .then(response => response.json())
    .then(response => {
      return response;
  }).catch(function(error) {
    console.log(error);
  });
  }