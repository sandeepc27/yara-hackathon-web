import React, { useState,useEffect } from 'react';
import {useNavigate} from "react-router-dom";
import "../styles/Login.css";
import l from "../assets/loginimg.png";
import logo from "../assets/logo.svg";
import {getEmployeeById} from '../services/apiEmployee';

function Login() {
    var usernameInput = React.createRef(); 
    var passwordInput = React.createRef();
    const [auth,setAuth] =useState([])
    const navigate = useNavigate();
    var doLogin = async() =>{
        var id=(usernameInput.current.value);
        var pass=(passwordInput.current.value);
        var a = await getEmployeeById(id);

        //Login authentication
        if(a.emppassword == pass && a.emprole == 'admin'){
            navigate("/Admin");}
        else if(a.emppassword == pass && a.emprole == 'employee'){
            navigate("/Employee/"+id);}
        else{
            alert("Enter correct username password");}
    }

    return (
        <div>
            <div className='header'>
                <div className='Logo'><img src={logo}/></div>
                <div className='text'><h2>Yara Yearly Hackathon</h2></div>
            </div>
            <div className='rowC'>
                <div className='image'><img src={l}/></div>
                <div className='login'> <h2>Login</h2><br/><br/>
                    <form>
                        Yara ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="name" ref={usernameInput} placeholder="Yara ID" /><br/><br/><br/>
                        Password:<input id="password" ref={passwordInput} type="password" placeholder="password" /><br/><br/><br/>
                        <div class="center"><input id="button" type="button" onClick={doLogin} value="Submit" /></div>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default Login