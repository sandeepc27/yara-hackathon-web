import React, { useState ,useEffect } from 'react';
import '../styles/NoDataStyles.css';
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router';

export default function NoTeamToVote() {
    const {id} = useParams();
  const navigate = useNavigate();
  const backtoEmployee = ()=>{
    navigate('/Employee/'+id);
  }

    return (
        <div classname='body'>
             <center><h1>Sorry.. No team has registered!!!</h1></center>
            <center><button onClick={()=>backtoEmployee()}>Back To Hackathon</button></center>
        </div>
    );
  };