import "../styles/RegDetailStyles.css";
import { useNavigate } from "react-router-dom";
import React,{Fragment,useState,useEffect} from 'react';
import Hero from "../component/Hero";
import Navbar from "../component/Navbar";
import { useParams } from 'react-router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Ongoing from "../component/Ongoing";
import {getTeam, deleteTeam,getTeamById,getTeamByHackthon} from "../services/apiTeam";
import TeamVotes from '../component/TeamVotes';
import WinningTeam from '../component/WinningTeam';

const RegDetails=()=>{
    const navigate = useNavigate();
    const [details,setDetails] = useState([]);
    const hackathonid = useParams();

    const loadDetails = async ()=>{
        setDetails(await getTeamByHackthon(hackathonid.id));
    }

    useEffect(()=>{
        loadDetails();
    },[]);

    //To check whether data is present
    const docheck=()=>
    {
        if(details.length==null)
            navigate('/NoTeamRegistered');
    }
    
    const doDelete = async (id) => {
        await deleteTeam(id);
        setDetails(await getTeam());
    }
    
    return(
        <div>
            <Hero
                cName="hero-mid"
                heroImg="https://drive.google.com/uc?export=view&id=1kieEpYSpesSFkeF7ebH4-GiGBIB_uFXD"
            />
            <Navbar/>
            <Tabs>
                <div className="tab">
                    <TabList>
                        <Tab><span>Team Details</span></Tab>
                        <Tab><span>Team Votes</span></Tab>
                        <Tab><span>Winning Team</span></Tab>
                    </TabList>
                </div>
                <TabPanel>
                <div>
                    <table className="tbl1">
                        <thead> 
                            <tr>
                                <th>#</th>
                                <th>Team Name</th>
                                <th>Problem Statement</th>
                                <th>Member Id</th>
                                <th>Member Name</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                           {docheck()}
                            {details.map((item) => {
                                return(
                                    <tr key={item.teamid}>
                                        <td>{item.teamid}</td>
                                        <td>{item.teamname}</td>
                                        <td>{item.problem}</td>
                                        <td><table className="tbl1"><tr></tr>
                                            {item.members.map(member => (
                                                <tr key={member.id}>
                                                    <td><center>{member.id}</center></td>
                                                    </tr>
                                            ))}
                                        </table></td>
                                        <td><table className="tbl1"><tr></tr>
                                            {item.members.map(member => (
                                                <tr key={member.id}>
                                                    <td><center>{member.name}</center></td>
                                                    </tr>
                                            ))}
                                        </table></td>
                                        <td><button  onClick={()=>doDelete(item.teamid)}>Delete</button></td>
                                    </tr>
                                )
                            }
                            )}
                        </tbody> 
                    </table>        
                </div>
            </TabPanel>
            <TabPanel>
                <div>      
                    <TeamVotes/>
                </div> 
            </TabPanel>
            <TabPanel>
                <div>      
                    <WinningTeam/>
                </div> 
            </TabPanel>
        </Tabs>
    </div>
)
}
export default RegDetails

