import React, { useState ,useEffect } from 'react';
import '../styles/AddTeamStyles.css';
import EmployeeNavbar from '../component/EmployeeNavbar'
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router';
import  {getTeamById,updateTeam} from "../services/apiTeam"

export default function EditTeam() {
  const navigate = useNavigate();
  const  teams  = useParams();
  const [items,setItems] = useState([]);
  const [formValues, setFormValues] = useState([{ id: "", name: ""}])
  const [team,setTeam]= useState({teamid:'',teamname:'',members:'',problem:'',hackathonid:'',created:'',updated:'2022-02-20'});
  

  const handleChange = (e) => {
    setTeam({...team,[e.target.placeholder.toLowerCase()]: e.target.value });
  }

  const updateEditTeam = async (hackId) =>{
    let team = await getTeamById(hackId);
    setTeam(team);
    setFormValues(team.members)
  }

  let handleChanges = (i, e) => {
    let newFormValues = [...formValues];
    newFormValues[i][e.target.name] = e.target.value;
    setFormValues(newFormValues);
  }

  let addFormFields = () => {
    setFormValues([...formValues, { name: "", id: "" }])
  }

  let removeFormFields = (i) => {
    let newFormValues = [...formValues];
    newFormValues.splice(i, 1);
    setFormValues(newFormValues)
  }

  useEffect(()=>{
    updateEditTeam(teams.teams);
  },[]);

  const handleSubmit = async (e) => {
    setTeam(team.members=formValues)
    const newItem = {...team};
    updateTeam(newItem);
    doCancel();
  }

  const doCancel = ()=>{
    navigate('/Employee/'+teams.id);
  }

  if(!team){
    return( <h3>No Team Detail is available</h3>)
  }
  return (
    <div>
      <EmployeeNavbar/>
        <div>
          <div className="hero">
            <img alt="background" src="https://drive.google.com/uc?export=view&id=1DJkaUxWmG7sivbKYMCro3Msi3gkXjnSv"/>
            <div className="hero-text">
              <center>
                <form className="form1">
                  <table className="table1">
                    <tr>
                      <td colSpan='2'><center><h2 className='h2class'>Edit Team</h2></center></td>
                    </tr>
                    <tr>
                      <td><h4>Team Name</h4></td>
                      <td><input placeholder='teamname' onChange={handleChange} value={team.teamname}/></td>
                    </tr>
                    <tr>
                      <td><h4>Members</h4></td>
                      <td>
                        <div>
                          <form>
                            <table>
                              {formValues.map((element, index) => (
                                <div className="form-inline" key={index}>
                                  <tr>
                                    <td><input type="text" name="id" value={element.id || ""} onChange={e => handleChanges(index, e)} /></td>
                                  </tr> 
                                  <td><input type="text" name="name" value={element.name || ""} onChange={e => handleChanges(index, e)} />
                                  {
                                    index ? 
                                    <button type="button"  className="button remove" onClick={() => removeFormFields(index)}>Remove</button> 
                                    : null
                                  }
                                  </td>
                                </div>
                              ))}
                              <div className="button-section">
                                <button className="button add" type="button" onClick={() => addFormFields()}>Add Members</button>
                              </div>
                            </table>
                          </form>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td><h4>Problem</h4></td>
                      <td><input placeholder='problem' onChange={handleChange} value={team.problem}/></td>
                    </tr>
                    <tr>
                      <td className='button-align'><button onClick={handleSubmit}>Update</button></td>
                      <td className='button-align'><button onClick={doCancel}>Cancel</button></td>
                    </tr>
                  </table>
                </form>
              </center>
            </div>
          </div>
        </div>
      </div>
    );
  };

