import React, { useState ,useEffect } from 'react';
import '../styles/AddHackathonstyles.css';
import Navbar from '../component/Navbar'
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router';
import  {addHackathon} from "../services/apiHackathon"

export default function AddHackathon() {
  const navigate = useNavigate();
  const  {hackId}  = useParams();
  const [hackathon,setHackathon]= useState({hackathonid:'',hackathonname:'',poster:'',hackathondate:'',venue:'',prize:'',winner:'',runner:''});

  const handleChange = (e) => {
    setHackathon({...hackathon,[e.target.placeholder.toLowerCase()]: e.target.value });
  }

  useEffect(()=>{
  },[]);

  const handleSubmit = async (e) => {
    const newItem = {...hackathon};
    console.log(newItem);
    addHackathon(newItem);
    navigate('/Admin');
    doCancel();
  }
  const doCancel = ()=>{
    navigate('/Admin');
  }
    return (
        <div>
             <Navbar/>
             <div>
                <div className="hero">
                  <img alt="background" src="https://drive.google.com/uc?export=view&id=1kieEpYSpesSFkeF7ebH4-GiGBIB_uFXD"/>
                    <div className="hero-text">
                      <center>
                        <form className="form1">
                          <table className="table1">
                            <tr>
                              <td colSpan='2'><center><h2>Add Hackathon</h2></center></td>
                            </tr>
                            <tr>
                              <td><h4>Hackathon Name</h4></td>
                              <td><input placeholder='HackathonName' onChange={handleChange} value={hackathon.hackathonname}/></td>
                            </tr>
                            <tr>
                              <td><h4>Poster Url</h4></td>
                              <td><input placeholder='Poster' onChange={handleChange} value={hackathon.poster}/></td>
                            </tr>
                            <tr>
                              <td><h4>Date</h4></td>
                              <td><input type="date" placeholder='HackathonDate' onChange={handleChange} value={hackathon.hackathondate}/></td>
                            </tr>
                            <tr>
                              <td><h4>Venue</h4></td>
                              <td><input placeholder='Venue' onChange={handleChange} value={hackathon.venue}/></td>
                            </tr>
                            <tr>
                              <td><h4>Prize</h4></td>
                              <td><input placeholder='Prize' onChange={handleChange} value={hackathon.prize}/></td>
                            </tr>
                            <tr>
                              <center><td  className='Addbutton'><button onClick={handleSubmit}>Add</button></td></center>
                              <td className='Addbutton'><button  onClick={doCancel}>Cancel</button></td>
                            </tr>
                            <br/>
                          </table>
                        </form>
                      </center>
                    </div>
                  </div>
                </div>
              </div>
   
    );
  };

