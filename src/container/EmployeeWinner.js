import "../styles/RegDetailStyles.css";
import { useNavigate } from "react-router-dom";
import React,{Fragment,useState,useEffect} from 'react';
import { useParams } from 'react-router';
import {getVotesCount} from "../services/apiVote";
import EmployeeNavbar from "../component/EmployeeNavbar";

const WinningTeam=()=>{
    const navigate = useNavigate();
    const [details,setDetails] = useState([]);
    const Empid = useParams();

    const loadDetails = async ()=>{
        setDetails(await getVotesCount(Empid.id));
    }

    useEffect(()=>{
        loadDetails();
    },[]);
    
    //To check whether data is present
    const docheck=()=>
    {
        if(details.length==null)
            {
            alert("No winners has been assigned");
            navigate('/Employee/'+Empid.id);
        }
    }

    const employee=()=>
    {
            navigate('/Employee/'+Empid.id);
    }
    
    return(
        
        <div>
            <div>  
                <center><h2>Hackathon Winners</h2></center>
            {docheck()}    
                {details.map(function(item,i){
                    if(i<3){
                        if(i%2==0){
                        return(
                            <div>
                                <div className="first-des">
                                <div className="des-text">
                                    <h2>{item.teamid}</h2>
                                    <h2>{item.teamname}</h2>
                                    <h2>Problem statement :<br/> {item.problem}</h2>
                                    <div>
                                        <table className="tbl1">
                                            <tr>
                                                <td>Employee ID</td>
                                                <td>Employee Name</td>
                                            </tr>
                                            {item.members.map(member => (
                                                <tr key={member.id}>
                                                    <td><center>{member.id}</center></td>
                                                    <td><center>{member.name}</center></td>
                                                </tr>
                                            ))}
                                        </table>
                                    </div><br/>
                                    <h2> Vote Count: {item.count}</h2>
                                </div>
                                <div className="images1">
                                    <img alt="img" src="https://drive.google.com/uc?export=view&id=17cH_LBpayJWmYbWx5lNibTK2uyQJ9RLX"/>
                                    <img alt="img" src="https://drive.google.com/uc?export=view&id=1DPBKmvtEfS_OUVZW6OmyUQ8JUFV8dOiW"/>
                                </div>
                            </div>
                            <br/>
                            <hr/>
                        </div>
                        )
                        }
                    else{
                        return(
                            <div className="first-des">
                                <div className="images1">
                                    <img alt="img" src="https://drive.google.com/uc?export=view&id=1aTAUKBWD9kIM4X9OEMyjaaRPjPfpCnJg"/>
                                    <img alt="img" src="https://drive.google.com/uc?export=view&id=19Rk7YSvmaCpeFlEsFAKnumads_c4EKp0"/>
                                </div>
                                <div className="des-text">
                                    <h2>{item.teamid}</h2>
                                    <h2>{item.teamname}</h2>
                                    <h2>Problem statement :<br/> {item.problem}</h2>
                                <div>
                                    <table className="tbl1">
                                        <tr>
                                            <td>Employee ID</td>
                                            <td>Employee Name</td>
                                        </tr>
                                        {item.members.map(member => (
                                            <tr key={member.id}>
                                                <td><center>{member.id}</center></td>
                                                <td><center>{member.name}</center></td>
                                            </tr>
                                        ))}
                                    </table>
                                </div><br/>
                            <h2> Vote Count: {item.count}</h2>
                            </div>
                        </div>
                    )
                }
            }
        })}
        <center><button onClick={()=>employee()} className='back-button'>Back To Hackathon</button></center>
        <br/> <br/>
    </div> 
</div>
)
}
export default WinningTeam

