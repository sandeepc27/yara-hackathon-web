import React, { useState ,useEffect } from 'react';
import '../styles/EditHackathonStyles.css';
import Navbar from '../component/Navbar'
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router';
import  {updateHackathon,getHackathonById} from "../services/apiHackathon"

export default function EditHackathon() {
  const navigate = useNavigate();
  const  hackId  = useParams();
  //const [items,setItems] = useState([]);
  const [hackathon,setHackathon]= useState({hackathonid:'',hackathonname:'',poster:'',hackathondate:'',venue:'',prize:'',winner:'',runner:''});

  const handleChange = (e) => {
    setHackathon({...hackathon,[e.target.placeholder.toLowerCase()]: e.target.value });
  }

  const updateEditHackathon = async (hackId) =>{
    console.log(hackId.id)
    let hackathon = await getHackathonById(hackId.id);
    setHackathon(hackathon); 
}

useEffect(()=>{
    if(hackId){ 
      updateEditHackathon(hackId);
    }
  },[]);

  const handleSubmit = async (e) => {
    const newItem = {...hackathon};
    updateHackathon(newItem);
    doCancel();
  }
  const doCancel = ()=>{
    navigate('/Admin');
  }

  const changeDateFormat = (d) => {
    let date = new Date(d);
    let result = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
    return result;
}
if(!hackathon){
    return( <h3>No Hackathon is registered yet</h3>)
}
    return (
        <div>
            <Navbar/>
            <div>
                <div className="hero">
                    <img alt="background" src="https://drive.google.com/uc?export=view&id=1kieEpYSpesSFkeF7ebH4-GiGBIB_uFXD"/>
                    <div className="hero-text">
                        <center>
                            <form className="form1">
                                <table className="table1">
                                    <tr>
                                        <td colSpan='2'><center><h2>Update Hackathon</h2></center></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Hackathon Name</h4></td>
                                        <td><input placeholder='HackathonName' onChange={handleChange} value={hackathon.hackathonname}/></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Poster Url</h4></td>
                                        <td><input placeholder='Poster' onChange={handleChange} value={hackathon.poster}/></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Date</h4></td>
                                        <td><input placeholder='HackathonDate' onChange={handleChange} value={changeDateFormat(hackathon.hackathondate)}/></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Venue</h4></td>
                                        <td><input placeholder='Venue' onChange={handleChange} value={hackathon.venue}/></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Prize</h4></td>
                                        <td><input placeholder='Prize' onChange={handleChange} value={hackathon.prize}/></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Winner</h4></td>
                                        <td><input placeholder='Winner' onChange={handleChange} value={hackathon.winner}/></td>
                                    </tr>
                                    <tr>
                                        <td><h4>Runner</h4></td>
                                        <td><input placeholder='Runner' onChange={handleChange} value={hackathon.runner}/></td>
                                    </tr>
                                    <tr>
                                    <td><button onClick={handleSubmit}>Update</button></td>
                                    <td><button onClick={doCancel}>Cancel</button></td>
                                    </tr>
                                </table>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>
   
    );
  };

