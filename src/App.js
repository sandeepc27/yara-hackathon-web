import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Login from './container/Login';
import Admin from './container/Admin';
import Employee from './container/Employee';
import AddHackathon from './container/AddHackathon';
import AddTeam from './container/AddTeam';
import EditTeam from './container/EditTeam';
import AlreadyVoted from './container/AlreadyVoted';
import RegDetails from './container/RegDetails';
import EditHackathon from './container/EditHackathon';
import EmployeeVote from './container/EmployeeVote';
import NoTeamRegistered from './container/NoTeamRegistered';
import NoTeamVote from './container/NoTeamVote';
import NoTeamToVote from './container/NoTeamToVote';
import EmployeeWinner from './container/EmployeeWinner';
import {QueryClientProvider, QueryClient} from 'react-query';


const queryClient = new QueryClient()

function App() {
  return (
    <QueryClientProvider client={queryClient}>
    <Router>
    <div style={{marginLeft: '10px'}}>
       <Routes>
         <Route exact path="/" element={<Login/>}/>
         <Route exact path="/Admin" element={<Admin/>}/>
         {/* <Route exact path="/Employee" element={<Employee/>}/> */}
         <Route exact path="/Employee/:id" element={<Employee/>}/>
         <Route exact path="/Login" element={<Login/>}/>
         <Route exact path="/AlreadyVoted/:id" element={<AlreadyVoted/>}/>
         <Route exact path="/NoTeamToVote/:id" element={<NoTeamToVote/>}/>
         <Route exact path="/Team/Add/:hackathonid/:id" element={<AddTeam/>}/>
         <Route exact path="/Team/Edit/:hackid/:teams/:id" element={<EditTeam/>}/>
         <Route exact path="/AddHackathon" element={<AddHackathon/>}/>
         <Route exact path="/Hackathon/Edit/:id" element={<EditHackathon/>}/>
         <Route exact path="/Hackathon/Detail/:id" element={<RegDetails/>}/>
         <Route exact path="/Vote/:hackathonid/:id" element={<EmployeeVote/>}/>
         <Route exact path="/NoTeamRegistered" element={<NoTeamRegistered/>}/>
         <Route exact path="/NoTeamVote" element={<NoTeamVote/>}/>
         <Route exact path="/EmployeeWinner/:hackathonid/:id" element={<EmployeeWinner/>}/>
       </Routes>
    </div>
   </Router>
   </QueryClientProvider>
  );
}

export default App;
