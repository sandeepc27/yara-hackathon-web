import React,{Fragment,useState,useEffect} from 'react';
import '../styles/OngoingStyles.css'
import { useNavigate } from "react-router-dom";
import { getOngoingHackathon } from '../services/apiHackathon';

const Ongoing=()=>{
    const navigate = useNavigate();
    const [detail,setDetail] = useState([])

    //To edit hackathon details
    const doEdit = (hackathonid) => {
        navigate('/Hackathon/Edit/'+hackathonid);
    }

    //To change date format
    const changeDateFormat = (d) => {
        let date = new Date(d);
        let month = date.getMonth()+1;
        let result = date.getDate() + '-' + month + '-' + date.getFullYear();
        return result;
    }

    const loadHackathons = async ()=>{
        setDetail(await getOngoingHackathon());
    }

    useEffect(()=>{
        loadHackathons();
    },[]);
    
    //To see all registered team
    const doFetch = (hackathonid) => {
        navigate('/Hackathon/Detail/'+hackathonid);
    }

    //to check if any hackathon is registered
    if(detail.length==0){
        return( <h3>No Ongoing Hackathon</h3>)
    }
    return( 
        <Fragment>
            <section className="expire">
                <div className="center">
                    <h3>Ongoing Hackathons</h3>
                </div>
                <div className="row">

                {
                  detail.map((detail)=>{
                    return(
                        <div className="column">
                        <div className="single-expire">
                            <div className="card">
                                <div onClick={()=>doFetch(detail.hackathonid)}>
                                <div className="expire-thumb">
                                    <div className="expire-tag">Ongoing</div>
                                    <img src={detail.poster} alt="poster"/>
                                </div>
                                <div className="expire-content">
                                    <h1>{detail.hackathonname}</h1>
                                </div>
                                </div>
                                <div className="expire-footer">
                                    <ul>
                                        <li>
                                            <img alt="calender" src="https://drive.google.com/uc?export=view&id=1kMbZOwJsDJrd3B8COrnO0kChyMJJ2DqS"/>&nbsp;&nbsp;
                                            <span>{changeDateFormat(detail.hackathondate)}</span>
                                        </li>
                                        <li>
                                        <img alt="trophy" src="https://drive.google.com/uc?export=view&id=11PLH_szHogJL6z5Pi9CahqkCka40ojTs"/>&nbsp;&nbsp;
                                        <span>{detail.prize}</span>

                                        </li>
                                    </ul>
                                    <ul>
                                        
                                        <li className='buttonli'>
                                        <center><button onClick={()=>doEdit(detail.hackathonid)}>Update</button></center>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    )
                  })  
                }  
                </div>
            </section>
        </Fragment>
    )
 }
 export default Ongoing;
