import { Component } from "react";
import "../styles/NavbarStyles.css"
import {MenuItems} from "./MenuItems";

class Navbar extends Component{
    state = {clicked: false };
    handleClick = ()=>{
        this.setState({clicked: !this.state.clicked})
    }
    render(){
        return(
            <nav className = "NavbarItems">
                <h1 className = "Navbar-logo">Yara Yearly Hackathon</h1>
                <div className="menu-icons" onClick={this.handleClick}>
                    <i className={this.state.clicked ? "fas fa-times" : "fas fa-bars"}></i>
                </div>
                <ul className = {this.state.clicked ?  "Nav-menu active" : "Nav-menu"}>
                    {MenuItems.map((item,index)=>{
                        return(
                            <li key={index}>
                                <a className={item.cName}
                                href={item.url}>{item.title}</a>
                            </li>
                        );
                    })}
                </ul>
            </nav>
        )
    }
}

export default Navbar;