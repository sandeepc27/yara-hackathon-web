import React,{Fragment,useState,useEffect} from 'react';
import '../styles/ExpireStyles.css'
import { useNavigate } from "react-router-dom";
import { getExpiredHackathon } from '../services/apiHackathon';

const Expire=()=>{
    const navigate = useNavigate();
    const [detail,setDetail] = useState([])

    const loadHackathons = async ()=>{
        setDetail(await getExpiredHackathon());
    }
    
    useEffect(()=>{
        loadHackathons();
    },[]);

    const changeDateFormat = (d) => {
        let date = new Date(d);
        let result = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
        return result;
    }

    //to check any hackathon is registered
    if(detail.length== 0){
        <h1>No Expired hackathons Yet</h1>
    }

    return( 
        <Fragment>
            <section className="expire">
                <div className="center">
                    <h3>Hackathons</h3>
                </div>
                <div  className="row">
                {
                  detail.map((detail)=>{
                    return(
                        <div className="column">
                        <div className="single-expire">
                            <div className="card">
                                <div className="expire-thumb">
                                    <div className="expire-tag">Expired</div>
                                    <img src={detail.poster} alt="poster"/>
                                </div>
                                <div className="expire-content">
                                    <h1>{detail.hackathonname}</h1>   
                                </div>
                                <div className="expire-footer">
                                    <ul>
                                        <li>
                                            <img alt="calender" src="https://drive.google.com/uc?export=view&id=1kMbZOwJsDJrd3B8COrnO0kChyMJJ2DqS"/>&nbsp;&nbsp;
                                            <span>{changeDateFormat(detail.hackathondate)}</span>
                                        </li>
                                        <li>
                                        <img alt="trophy" src="https://drive.google.com/uc?export=view&id=11PLH_szHogJL6z5Pi9CahqkCka40ojTs"/>&nbsp;&nbsp;
                                        <span>{detail.prize}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    )
                  })  
                }  
                </div>
            </section>
        </Fragment>
    )
 }
 export default Expire;
