import React,{Fragment,useState,useEffect} from 'react';
import '../styles/OngoingStyles.css'
import { useNavigate } from "react-router-dom";
import { getOngoingHackathon } from '../services/apiHackathon';
import { useParams } from 'react-router-dom';
import {getEmployeeBySearch} from '../services/apiTeam';

const Ongoing=()=>{
    const navigate = useNavigate();
    const [detail,setDetail] = useState([]);
    const [employee,setEmployee] = useState([]);
    const {id}=useParams();

    const loadHackathons = async ()=>{
        setDetail(await getOngoingHackathon());
    }

    useEffect(()=>{
        loadHackathons();
    },[]);

    //if new add..else edit
    const doFetch = async(hackathonid)  => {
        let a=await getEmployeeBySearch(hackathonid,id);
        if(a.length==0){
            navigate('/Team/Add/'+hackathonid+'/'+id);
        }
        else{
            let teams=a[0].teamid
            navigate('/Team/Edit/'+hackathonid+'/'+teams+'/'+id);
        }
    }

    //Change to date format
    const changeDateFormat = (d) => {
        let date = new Date(d);
        let result = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
        return result;
    }

    //to navigate to vote page
    const doVote = async(hackathonid)  => {
        navigate('/Vote/'+hackathonid+'/'+id);
    }

    const seeWinner = async(hackathonid)  => {
        navigate('/EmployeeWinner/'+hackathonid+'/'+id);
    }

    if(detail.length==null){
        return( <h3> No Ongoing Hackathon</h3>)
    }
    return( 
        <Fragment>
            <section className="expire">
                <div className="center">
                    <h3>Ongoing Hackathons</h3>
                </div>
                <div  className="row">
                {
                  detail.map((detail)=>{
                    return(
                        <div className="column">
                        <div className="single-expire">
                            <div className="card">
                                <div onClick={()=>doFetch(detail.hackathonid)}>
                                <div className="expire-thumb">
                                    <div className="expire-tag">Ongoing</div>
                                    <img src={detail.poster} alt="poster"/>
                                </div>
                                <div className="expire-content">
                                    <h1>{detail.hackathonname}</h1>
                                </div>
                                </div>
                                <div className="expire-footer">
                                    <ul>
                                        <li>
                                            <img alt="calender" src="https://drive.google.com/uc?export=view&id=1kMbZOwJsDJrd3B8COrnO0kChyMJJ2DqS"/>&nbsp;&nbsp;
                                            <span>{changeDateFormat(detail.hackathondate)}</span>
                                        </li>
                                        <li>
                                        <img alt="trophy" src="https://drive.google.com/uc?export=view&id=11PLH_szHogJL6z5Pi9CahqkCka40ojTs"/>&nbsp;&nbsp;
                                        <span>{detail.prize}</span>
                                        </li>
                                    </ul>
                                    <ul>
                                        <li>
                                        <button onClick={()=>doVote(detail.hackathonid)}>Vote</button>
                                        </li>
                                        <li className='back-button'>
                                        <button onClick={()=>seeWinner(detail.hackathonid)}>View winner</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    )
                  })  
                }  
                </div>
            </section>
        </Fragment>
    )
 }
 export default Ongoing;
