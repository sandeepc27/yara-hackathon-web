import Table from 'react-bootstrap/Table'
import "../styles/RegDetailStyles.css";
import { useNavigate } from "react-router-dom";
import React,{Fragment,useState,useEffect} from 'react';
import Hero from "../component/Hero";
import { useParams } from 'react-router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {getVotesCount} from "../services/apiVote";

const TeamVotes=()=>{
    const navigate = useNavigate();
    const [details,setDetails] = useState([]);
    const hackathonid = useParams();

    const loadDetails = async ()=>{
        setDetails(await getVotesCount(hackathonid.id));
    }

    useEffect(()=>{
        loadDetails();
      },[]);
    
    //To check whether data is present
    const docheck=()=>
    {
        if(details.length==null)
            navigate('/NoTeamVote');
    }

return(
    <div>
        <div>
        <table className="tbl1">
           <thead> 
                <tr>
                    <th>Team Id</th>
                    <th>Team Name</th>
                    <th>Problem Statement</th>
                    <th>Member Id</th>
                    <th>Member Name</th>
                    <th>Vote</th>
                </tr>
            </thead>
             <tbody>
             {docheck()}
                {details.map((item) => {
                    return(
                        <tr key={item.teamid}>
                            <td>{item.teamid}</td>
                            <td>{item.teamname}</td>
                            <td>{item.problem}</td>
                            <td>
                                <table className="tbl1"><tr></tr>
                                    {item.members.map(member => (
                                    <tr key={member.id}>
                                        <td><center>{member.id}</center></td>
                                    </tr>
                                    ))}
                                </table>
                            </td>
                            <td>
                                <table className="tbl1"><tr></tr>
                                {item.members.map(member => (
                                    <tr key={member.id}>
                                        <td><center>{member.name}</center></td>
                                        </tr>
                                ))}
                                </table>
                            </td>
                            <td>{item.count}</td>
                        </tr>
                    )
                }
                )}
            </tbody> 
        </table>        
        </div>
    </div>

    )
}
export default TeamVotes

